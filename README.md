**Swoole** is a production-grade async programming framework for PHP. It is a PHP extension written in pure C language, which enables PHP developers to write high-performance, scalable, concurrent TCP, UDP, Unix socket, HTTP, WebSocket services in PHP without too much knowledge of the non-blocking I/O programming and low-level Linux kernel. You can think of Swoole as something like NodeJS but for PHP, with higher performance.

***Why run Laravel on Swoole?***

The image below illustrates the lifecycle in PHP. As you can see, when you run php script every time, PHP needs to initialize modules and launch Zend Engine for your running environment. And your PHP script needs to be compiled to OpCodes for Zend Engine’s execution.

However, this lifecycle needs to go over and over again in each request. Because the environment created for a single request will be immediately destroyed after the request process is done.

In other words, in traditional PHP lifecycle, it wastes a bunch of time building and destroying resources for your script execution. And imagine in frameworks like Laravel, how many files does it need to load for one request? There’s a lot of I/O consumption for loading files as well.

---
MIT (c) 20202 Pichate Ins
