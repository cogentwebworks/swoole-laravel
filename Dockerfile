FROM docker.io/phpswoole/swoole:4.4.19-php7.4

RUN apt-get update && apt-get install -y \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libmcrypt-dev \
    libpng-dev \
    zlib1g-dev \
    libxml2-dev \
    libzip-dev \
    libonig-dev \
    graphviz  \
    libpq-dev

RUN docker-php-ext-configure gd \
    && docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql \
    && docker-php-ext-install \
    mbstring \
    mysqli \
    pgsql \
    pdo_mysql \
    pdo_pgsql \
    opcache \
    zip \
    gd \ 
    && docker-php-source delete

RUN mkdir -p /var/log/cron \
    && touch /var/log/cron/cron.log \
    && mkdir -m 0644 -p /etc/cron.d \
    && chown -R www-data:www-data /var/www

RUN composer global require hirak/prestissimo

ARG HOMEDIR=/var/www/html
ENV HOMEDIR=$HOMEDIR
WORKDIR $HOMEDIR
ENV PATH="$PATH:/var/www/html/vendor/bin"

EXPOSE 1215